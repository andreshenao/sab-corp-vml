<header id="navbar" role="banner" class="<?php print $navbar_classes; ?>">
  <div class="<?php print $container_class; ?>">
    <div class="navbar-header">
      <?php if ($logo): ?>
        <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>

      <?php if (!empty($site_name)): ?>
        <a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
      <?php endif; ?>

      <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
          <span class="sr-only"><?php print t('Toggle navigation'); ?></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      <?php endif; ?>
      <div class="logo2" />
      </div>

    <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
      <div class="navbar-collapse collapse" id="navbar-collapse">
        <nav role="navigation">
          <?php if (!empty($primary_nav)): ?>
            <?php print render($primary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($secondary_nav)): ?>
            <?php print render($secondary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($page['navigation'])): ?>
            <?php print render($page['navigation']); ?>
          <?php endif; ?>
        </nav>
      </div>
    <?php endif; ?>
  </div>
  <?php if(isset($node->field_header)): ?>
    <?php if(!empty($node->field_header)): ?>
        <div class="title-header">
        <?php print($node->field_header["und"][0]["value"]); ?>
        </div>
      <?php endif; ?>
  <?php endif; ?>
</header>

<div class="drinkingslider"><!-- /.region del slider -->
    <?php print render($page['page_slider']); ?>
</div>

<div class="drinkingform"><!-- /.region del formulario -->
    <?php print render($page['page_top']); ?>

    <?php print render($page['brandslider']); ?>
    <form>
        <label>Gender</label>
        <input type="text">
        <label>Age</label>
        <input type="text">
        <label>Serving size</label>
        <input type="text">
        <label>Number of drinks per week </label>
        <input type="text">
        <label>Total Units  </label>
        <input type="text">
    </form>
</div>

<div class="drinkingnews"><!-- /.region de news -->
    <?php print render($page['news_slider']); ?>
</div>


<?php if (!empty($page['footer'])): ?>
  <footer class="footer <?php print $container_class; ?>">
    <?php print render($page['footer']); ?>
  </footer>
<?php endif; ?>
