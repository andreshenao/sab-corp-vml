/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


jQuery(document).ready(function ($) {
    var isScreenMobile = $(window).width() < 992;

    // -- General -- //
    var $GeneralScope = {
        // Constructor
        init: function init() {
            this.menuScripts();
            this.navigationSnippet();
            this.setBackgroundImage();
            this.searchDesktop();
            this.sliderTabs();
            // this.jpaginate();
        },

        //jpaginate
        jpaginate: function jpaginate() {
            $(".jpaginate-3").paginate({
                items_per_page: 3
            });
            $(".jpaginate-6").paginate({
                items_per_page: 6
            });
            $(".jpaginate-9").paginate({
                items_per_page: 9
            });

            $(".wrapper-date .one").on("click", function () {
                // console.log("debug .....");
                $("form.ctools-auto-submit-full-form").change();
                $("form.ctools-auto-submit-full-form").add(".ctools-auto-submit").filter("form, select, input:not(:text, :submit)").once("ctools-auto-submit").change(function (e) {
                    // don't trigger on text change for full-form
                    if ($(e.target).is(":not(:text, :submit, .ctools-auto-submit-exclude)")) {
                        triggerSubmit.call(e.target.form);
                    }
                });
            });
        },

        // Menu scripts
        menuScripts: function menuScripts() {
            var ButtonTrigger = $(".navbar-header .navbar-toggle"),
                LogoMenu = $(".navbar-header .logo"),
                MenuWrapper = $(".navbar-header .navbar-collapse"),
                NavHeader = $(".navbar-header");

            if (ButtonTrigger) {
                $(document).on("click", ".navbar-toggle", function () {
                    LogoMenu.toggleClass("open-menu");
                    ButtonTrigger.toggleClass("collapsed open-menu");
                    NavHeader.toggleClass("open-menu");
                    $("body").toggleClass("fixed");
                });
            }
        },

        navigationSnippet: function navigationSnippet() {
            $('a[href*="#"]')
            // Remove links that don't actually link to anything
            .not('[href="#"]').not('[href="#0"]').click(function (event) {
                var _this = this;

                // On-page links
                if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
                    (function () {
                        // Figure out element to scroll to
                        var target = $(_this.hash);

                        target = target.length ? target : $("[name=" + _this.hash.slice(1) + "]");
                        // Does a scroll target exist?
                        if (target.length) {
                            // Only prevent default if animation is actually gonna happen
                            event.preventDefault();
                            $("html, body").animate({
                                scrollTop: target.offset().top
                            }, 1000, function () {
                                // Callback after animation
                                // Must change focus!
                                var $target = $(target);
                                $target.focus();

                                if ($(window).width() <= 768) {
                                    menu_wrapper.slideUp(300);
                                }

                                if ($target.is(":focus")) {
                                    // Checking if the target was focused
                                    return false;
                                } else {
                                    $target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
                                    $target.focus(); // Set focus again
                                }
                            });
                        }
                    })();
                }
            });
        },

        setBackgroundImage: function setBackgroundImage() {
            // let Body = $("body"),
            //     DataBackgroundDesktop = Body.data("bg-desktop"),
            //     HeaderMain = $("header.navbar-default");
            var Body = $("body"),
                DataBackgroundDesktop = Body.data("bg-desktop"),
                DataBackgroundMobile = Body.data("bg-mobile"),
                HeaderMain = $("header.navbar-default");

            // HeaderMain.attr("style", 'background-image:url("' + DataBackgroundDesktop + '")');
            if ($(window).width() <= 991) {
                if (DataBackgroundMobile != "") {
                    HeaderMain.attr("style", 'background-image:url("' + DataBackgroundMobile + '")');
                } else {
                    HeaderMain.attr("style", 'background-image:url("' + DataBackgroundDesktop + '")');
                }
            } else {
                HeaderMain.attr("style", 'background-image:url("' + DataBackgroundDesktop + '")');
            }
        },

        searchDesktop: function searchDesktop() {
            var btnSearch = "#navbar-collapse > nav > ul > li.last.leaf > a",
                $blockSearch = $(".block-search");

            $(document).on("click", btnSearch, function (e) {
                e.preventDefault();

                $blockSearch.toggleClass("open-search");
            });
        },

        sliderTabs: function sliderTabs() {
            var $sliderTabs = $(".container-tab");

            if ($sliderTabs.length) {
                // Set first item
                if (isScreenMobile) {
                    // Tabs About Us
                    if ($("body").hasClass("sabc-ourvision")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1');
                    } else if ($("body").hasClass("sabc-leadership")) {
                        $(".container-tab .tab-3").insertBefore('.container-tab .tab-1');
                    } else if ($("body").hasClass("sabc-contrib")) {
                        $(".container-tab .tab-4").insertBefore('.container-tab .tab-1');
                    }

                    // Tabs Our Community
                    if ($("body").hasClass("sabc-transformation")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1');
                    } else if ($("body").hasClass("sabc-sustainability")) {
                        $(".container-tab .tab-3").insertBefore('.container-tab .tab-1');
                    } else if ($("body").hasClass("sabc-smartdrinking")) {
                        $(".container-tab .tab-4").insertBefore('.container-tab .tab-1');
                    }

                    // Tabs Our Stories
                    if ($("body").hasClass("sabc-125years")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1');
                    } else if ($("body").hasClass("documents")) {
                        $(".container-tab .tab-3").insertBefore('.container-tab .tab-1');
                    } else if ($("body").hasClass("custom-gallery")) {
                        $(".container-tab .tab-4").insertBefore('.container-tab .tab-1');
                    }

                    // Tabs Type of Documents
                    if ($("body").hasClass("images-logos")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1');
                    } else if ($("body").hasClass("approved-video")) {
                        $(".container-tab .tab-3").insertBefore('.container-tab .tab-1');
                    }

                    // Tabs Contact Us
                    if ($("body").hasClass(" ")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1');
                    } else if ($("body").hasClass(" ")) {
                        $(".container-tab .tab-3").insertBefore('.container-tab .tab-1');
                    }

                    // Tabs Legal
                    if ($("body").hasClass("sabc-ourdream")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1');
                    } else if ($("body").hasClass("sabc-cookiepolicy")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1');
                    } else if ($("body").hasClass("sabc-privacypolicy")) {
                        $(".container-tab .tab-3").insertBefore('.container-tab .tab-1');
                    }

                    // Tabs Contact Us
                    if ($("body").hasClass("sabc-siteslocation")) {
                        $(".container-tab .tab-2").insertBefore('.container-tab .tab-1');
                    }
                }

                $sliderTabs.slick({
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    responsive: [{
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    }]
                });
            }
        }
    };

    // -- Agegate -- //
    var $AgegateScope = {
        // Constructor
        init: function init() {
            this.ageScripts();
            this.moveSectionFooterIntoFooter();
        },

        // scripts for Agegate
        ageScripts: function ageScripts() {
            // Dom manipulation
            var ListCountries = $(".form-item-list-of-countries"),
                Checklist = $(".form-item-remember-me"),
                FbValidate = $(".age_checker_facebook_validate"),
                RememberMe = $(".ab-inbev-remember-me"),
                RememberLabel = Checklist.find("label"),
                RememberMeStr = RememberMe.find("strong"),
                FooterContent = $(".ab-inbev-footer");

            if (ListCountries) {
                ListCountries.insertAfter("#age_checker_error_message");
            }

            // if(Checklist) {
            //  Checklist.append(RememberMe)
            //  RememberLabel.append(RememberMeStr)
            // }

            // if(FbValidate){
            //  FbValidate.insertAfter('#edit-submit')
            //  FbValidate.append('<span class="fbTrigger">Sign in with <b>facebook</b></span>')
            // }

            if (FooterContent) {
                $('.agegate-container-footer').append(FooterContent);
            }
        },

        moveSectionFooterIntoFooter: function moveSectionFooterIntoFooter() {
            var SectionMenu = $("#block-menu-menu-footer-menu");

            if (SectionMenu) {
                SectionMenu.insertAfter(".ab-inbev-footer .ab-inbev-footer-content-1 .ab-inbev-footer-aware-logo");
                $(".ab-inbev-footer-content-1 .menu-wrapper").hide();
            }
        }
    };

    // -- Home -- //
    var $HomeScope = {
        // Constructor
        init: function init() {
            // Instance functions
            this.sliderVideos();
            this.maxTextNews();
            this.heroSliders();
        },

        heroSliders: function heroSliders() {
            $('.view-banners > .view-content').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                draggable: false,
                asNavFor: '.view-mini-banners > .view-content'
            });
            $('.view-mini-banners .view-content').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                asNavFor: '.view-banners > .view-content',
                dots: false,
                arrows: false,
                centerMode: false,
                focusOnSelect: true,
                autoplay: true,
                autoplaySpeed: 3000,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        autoplay: true,
                        autoplaySpeed: 3000,
                        dots: false,
                        arrows: false
                    }
                }]
            });
            jQuery('.view-mini-banners .view-content .field-content .views-field-name a').click(function (e) {
                e.preventDefault();
            });
        },

        maxTextNews: function maxTextNews() {
            var textTitle = $('#block-views-news-block .views-row .views-field-title a'),
                textParagraph = $('#block-views-news-block .views-row .views-field-body p');

            textTitle.each(function () {
                $(this).text($(this).text().substring(0, 68) + '...');
            });

            textParagraph.each(function () {
                $(this).text($(this).text().substring(0, 250) + '...');
            });
        },

        sliderVideos: function sliderVideos() {
            var $sliderVideos = $(".view-slider-video .slider-component");

            $sliderVideos.find('.views-field-title').append('<span class="titles"></span>');

            $sliderVideos.on('init afterChange', function (event, slick, currentSlide, nextSlide) {
                var actualSlide = $(".view-slider-video .slick-current"),
                    prevTitle = actualSlide.prev().find('.views-field-title .field-content > p').text(),
                    nextTitle = actualSlide.next().find('.views-field-title .field-content > p').text();

                actualSlide.find('.views-field-title .titles').empty().html('<p>' + prevTitle + '</p><p>' + nextTitle + '</p>');
            });

            $sliderVideos.slick({
                infinite: true
            });

            // let listVideo = document.querySelectorAll(
            //  ".view-id-slider_video .slick-track .views-row"
            // );

            // for (let index = 0; index < listVideo.length; index++) {
            //  const element = listVideo[index];
            //  if (element.nextSibling != null) {
            //      let textNext = element.nextSibling.firstElementChild.innerText;
            //      let p = document.createElement("p");
            //      p.innerText = textNext;
            //      element.children[0].children[0].appendChild(p);
            //  }
            // }

            $sliderVideos.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $('.slick-current iframe').attr('src', $('.slick-current iframe').attr('src'));
            });
        }
    };

    // -- News --//
    var $NewsScope = {
        init: function init() {
            this.filter();
            this.temporalPaginator();
        },

        filter: function filter() {
            var listItem = document.querySelectorAll(".view-id-related_articles .view-content .views-field-field-news-date .date-display-single");
            var listYear = document.querySelectorAll(".wrapper-date .year");
            var valueYear = "0";
            var NumberItemsPaginate = document.querySelector(".wrapper-date").attributes["data-number-item"].value;
            var arraylist = [];

            //Array save data of start-list
            arraylist = listItem;
            //Event click
            for (var index = 0; index < listYear.length; index++) {
                var element = listYear[index];
                element.addEventListener("click", function (e) {
                    var dataYear = e.target.attributes["data-year"].value;
                    Dofilter(dataYear, arraylist);
                });
            }

            //Function Do filter year and start-list
            var Dofilter = function Dofilter(year, list) {
                var count = 0;
                var countNotFound = 0;

                //console.log("list request .. ", list);
                for (var index = 0; index < list.length; index++) {
                    var element = list[index];
                    var row = element.closest(".views-row");
                    var viewContent = document.querySelector(".view-related-articles .view-content ul");
                    var para = document.createElement("p");

                    if (element.textContent != year && year != "0") {
                        row.remove();
                        countNotFound++;
                        //Not result
                        if (count == 0 && countNotFound == list.length) {
                            var notFoundNot = document.querySelector(".not-found");
                            var control = document.querySelector(".pagination__controls");
                            if (control != null) {
                                control.remove();
                            }
                            //only one not repeat text
                            if (notFoundNot == null) {
                                para.innerText = "Not found";
                                para.classList.add("not-found");
                                viewContent.appendChild(para);
                            }
                        }
                    } else {
                        var notFoundNot = document.querySelector(".not-found");
                        viewContent.appendChild(row);
                        count++;
                        if (notFoundNot != null) {
                            notFoundNot.remove();
                        }
                        if (count > NumberItemsPaginate) {
                            viewContent.classList.add("jpaginate-filter");
                        } else {
                            var control = document.querySelectorAll(".pagination__controls");
                            viewContent.classList.remove("jpaginate-filter");
                            viewContent.classList.add("less-3");
                            if (control != null) {
                                for (var _index = 0; _index < control.length; _index++) {
                                    var _element = control[_index];
                                    _element.remove();
                                }
                            }
                        }
                    }
                }
                var children = document.querySelectorAll(".less-3 li");
                if (children != null) {
                    for (var index = 0; index < children.length; index++) {
                        var element = children[index];
                        element.style.display = "block";
                    }
                }
                $(".jpaginate-filter").paginate({
                    items_per_page: NumberItemsPaginate,
                    prev_next: false
                });
            };
            Dofilter(valueYear, arraylist);
        },

        temporalPaginator: function temporalPaginator() {
            var slider = document.querySelector('.sabc-news .pagination');
            var isDown = false;
            var startX = undefined;
            var scrollLeft = undefined;

            slider.addEventListener('mousedown', function (e) {
                isDown = true;
                slider.classList.add('active');
                startX = e.pageX - slider.offsetLeft;
                scrollLeft = slider.scrollLeft;
            });

            slider.addEventListener('mouseleave', function () {
                isDown = false;
                slider.classList.remove('active');
            });

            slider.addEventListener('mouseup', function () {
                isDown = false;
                slider.classList.remove('active');
            });

            slider.addEventListener('mousemove', function (e) {
                if (!isDown) return;
                e.preventDefault();
                var x = e.pageX - slider.offsetLeft;
                var walk = (x - startX) * 3; //scroll-fast
                slider.scrollLeft = scrollLeft - walk;
            });

            $(document).on('click', '.sabc-news .pagination li', function () {
                window.scrollTo(0, 500);
            });
        }
    };

    // -- Video Approved --//
    var $VideoScope = {
        init: function init() {
            this.video();
        },

        video: function video() {
            $("button, input[type='button'], .close").on("click", function (event) {
                var ci = $(this).attr("cdata");
                $(".media-youtube-" + ci + " .media-youtube-player")[0].contentWindow.postMessage('{"event":"command","func":"' + "stopVideo" + '","args":""}', "*");
            });
            $(".file-video-youtube").on("click", function (e) {
                $(this).parents(".views-field-field-video-youtube").siblings(".views-field-nothing").find(".sabc-cgallery-showvideo").click();
            });
            $(".sabc-cgallery-showvideo").on("click", function (event) {
                event.preventDefault();
                var racmodal = $(this).attr("cdata");
                $("#" + racmodal).modal({
                    backdrop: "static",
                    keyboard: false
                });
                $("#" + racmodal).modal("show");
            });
        }
    };

    // -- Brands --//
    var $BrandsScope = {
        init: function init() {
            this.slickBrands();
        },

        slickBrands: function slickBrands() {
            var mainSlider = ".page-brands #block-system-main > div > div.view-content > div > ul",
                listSlider = ".page-brands #block-system-main > div > div.attachment.attachment-after > div > div > div > ul";

            $(mainSlider).slick({
                adaptiveHeight: true,
                infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                asNavFor: listSlider
            });

            $(listSlider).slick({
                infinite: false,
                arrows: true,
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: mainSlider,
                focusOnSelect: true,
                responsive: [{
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4
                    }
                }, {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3
                    }
                }, {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2
                    }
                }]
            });
        }
    };

    // -- Temporal News --//
    var $TemporalNewsScope = {
        init: function init() {
            this.generalScript();
        },

        generalScript: function generalScript() {
            var createListToSelect = function createListToSelect() {
                var $formYears = $("#views-exposed-form-test-news-page"),
                    $listYears = $("#edit-field-news-date-value-value-year"),
                    $newContainerYear = $("#news-filter-years");
                // arrayYears = []

                // $formYears.attr("method", "post")
                $newContainerYear.empty();

                $listYears.find("option").each(function (i, year) {
                    //if (year.value != "" && i != $listYears.find("option").length - 1)
                    if (year.value != "") {
                        var classActive = "";
                        if (year.selected) {
                            classActive = "active-year";
                        }
                        // arrayYears.push(year.value)
                        //console.log(year);
                        $newContainerYear.append('<div class="year ' + classActive + '" data-value="' + year.value + '">' + year.value + '</div>');
                    }
                });

                $newContainerYear.removeClass('slick-initialized slick-slider').slick({
                    slidesToShow: 5,
                    slidesToScroll: 5
                });

                $(document).on('click', '#news-filter-years .year', function (e) {
                    //$(this).addClass("active-year").siblings().removeClass("active-year");
                    var $el = $(e.currentTarget);
                    // $indexSlick = $el.data('slick-index')

                    $listYears.find("option[value='" + $el.data('value') + "']").attr("selected", "selected");
                    $formYears.find('.form-submit').click();
                    // $formYears.submit();

                    $(document).ajaxComplete(function () {
                        createListToSelect();

                        $newContainerYear.slick({
                            slidesToShow: 5,
                            slidesToScroll: 5
                        });
                    });
                });

                // let select = document.querySelectorAll(".sabcnews .date-year")[0].childNodes[0];

                // /* Create list */
                // for (let index = 0; index < select.length; index++) {
                //  const textYear = select[index].value;

                //  if(document.getElementById("news-filter-years").childElementCount <= select.length){
                //      let node = document.createElement("li");
                //      let textnode = document.createTextNode(textYear);

                //      node.appendChild(textnode);
                //      document.getElementById("news-filter-years").appendChild(node);
                //  }
                // }

                // let list = document.querySelectorAll("#news-filter-years li");

                // for (let index = 0; index < list.length; index++){
                //  const element = list[index];

                //  element.addEventListener("click", function (e){
                //      for (let index = 0; index < select.length; index++){
                //          const element = select[index];

                //          if (element.value == e.target.textContent) {
                //              element.setAttribute("selected", "selected");

                //              let formSubmit = document.querySelector("#edit-submit-test-news");

                //              formSubmit.click();

                //              $(document).ajaxComplete(function() {
                //                  createListToSelect();
                //              });
                //          }
                //      }
                //  });
                // }
            };

            createListToSelect();
        }
    };

    var $highlightSearched = {
        init: function init() {
            this.highlightSearched();
        },

        highlightSearched: function highlightSearched() {
            var term = jQuery('#edit-name').val().trim().toLowerCase();
            if (term.length > 0) {
                var source = jQuery('.accordion .card').html();
                var words = source.split(' ');
                var output = '';
                jQuery.each(words, function (idx, word) {
                    if (term === word.toLowerCase()) {
                        output += '<span class="searched-red">' + word + '</span> ';
                    } else {
                        output += word + ' ';
                    }
                });

                jQuery('.accordion .card').html(output);
            }
        }
    };

    var $sliderSmartDrinkink = {
        init: function init() {
            this.sliderSmartDrinkink();
        },
        sliderSmartDrinkink: function sliderSmartDrinkink() {
            var sliderSD = jQuery('.view-news.view-display-id-block_1 .view-content');
            var sliderNews = jQuery('.view-news.view-display-id-block_news_harm_reduction .view-content');
            var sliderMar = jQuery('.view-sabc.view-display-id-block_responsible_marketing .view-content');
            var sliderDri = jQuery('.view-sabc.view-display-id-block_responsible_driving .view-content');
            var sliderCom = jQuery('.view-sabc.view-display-id-block_responsible_communities .view-content');
            var sliderT = jQuery('.view-drinking-brands.view-id-drinking_brands .view-content');
            var mediaQuery = $(window).width() > 992;
            if (mediaQuery == true) {
                sliderSD.slick({
                    arrows: true,
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1
                });
            }
            sliderT.slick({
                arrows: true,
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            });
            sliderNews.slick({
                arrows: true,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1
            });
            sliderMar.slick({
                arrows: true,
                infinite: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        variableWidth: true
                    }
                }]
            });
            sliderDri.slick({
                arrows: true,
                infinite: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        variableWidth: true
                    }
                }]
            });
            sliderCom.slick({
                arrows: true,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        }
    };

    // ----------------------------
    // TRIGGERS
    // ----------------------------

    // Trigger
    $GeneralScope.init();

    // Agegate
    if ($("body").hasClass("page-agegate")) {
        $AgegateScope.init();
    }

    // Home Scripts
    if ($("body").hasClass("front")) {
        $HomeScope.init();
    }

    // News Scripts
    if ($("body").hasClass("sabc-news")) {
        $NewsScope.init();
    }

    // Temporal News Scripts
    if ($("body").hasClass("page-news")) {
        $TemporalNewsScope.init();
    }

    // Video Scripts
    if ($("body").hasClass("approved-video")) {
        $VideoScope.init();
    }

    // Brands Scripts
    if ($("body").hasClass("page-brands")) {
        $BrandsScope.init();
    }

    // Documents Scripts
    if ($("body").hasClass("documents")) {
        $highlightSearched.init();
    }

    // Documents Scripts
    if ($("body").hasClass("page-smark-drinking") || $("body").hasClass("sabc-comunitites") || $("body").hasClass("sabc-marketing") || $("body").hasClass("sabc-driving") || $("body").hasClass("sabc-retailing")) {
        $sliderSmartDrinkink.init();
    }

    jQuery(document).ajaxComplete(function () {
        var sliderMar = jQuery('.view-sabc.view-display-id-block_responsible_marketing .view-content');
        var sliderDri = jQuery('.view-sabc.view-display-id-block_responsible_driving .view-content');
        sliderMar.slick({
            arrows: true,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    variableWidth: true
                }
            }]
        });
        sliderDri.slick({
            arrows: true,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    variableWidth: true
                }
            }]
        });
    });

    $(document).on('click', '.item-title-smart-slide', function () {
        $(".item-title-smart-slide").removeClass("activebrand");
        $(this).addClass("activebrand");
        localStorage.setItem('size', $(this).data("size"));
        $(".img-brand-smart-drinks").html($(this).find(".imgbrandsslider").html());
        $(".inputsize").val($(this).data("size") + " ml");
        $("#block-block-116 .block-title").html($(this).find(".item-title-brand").html());
        $(".inputunits").val("");
    });

    if ($(window).width() <= 600) {
        $(document).on('click', '.slick-next , slick-prev', function () {
            localStorage.setItem('size', $("#block-views-drinking-brands-block .slick-list .slick-active .item-title-smart-slide").data("size"));
            $(".inputsize").val($("#block-views-drinking-brands-block .slick-list .slick-active .item-title-smart-slide").data("size") + " ml");
            $("#block-block-116 .block-title").html($("#block-views-drinking-brands-block .slick-active .item-title-brand").html());
            $(".img-brand-smart-drinks").html($("#block-views-drinking-brands-block .slick-active .imgbrandsslider").html());
            $(".inputunits").val("");
        });
    }
    $(document).on('click', '.item-title-smart-slide', function () {
        $(".item-title-smart-slide").removeClass("activebrand");
        $(this).addClass("activebrand");
        localStorage.setItem('size', $(this).data("size"));
        $(".img-brand-smart-drinks").html($(this).find(".imgbrandsslider").html());
        $(".inputsize").val($(this).data("size") + " ml");
        $("#block-block-116 .block-title").html($(this).find(".item-title-brand").html());
        $(".inputunits").val("");
    });

    $(document).on('click', '.btn-smart-drinks', function () {
        if (localStorage.getItem("size") === null) {
            $("#form-smart-drinks legend").html("Please select one brand");
        } else {
            var size = localStorage.getItem('size');
            var inputweek = $(".inputweek").val();
            if (size != "" && inputweek != "") {
                var cal = parseInt(size) + parseInt(inputweek) / 1000;
                $(".inputunits").val(cal);
            } else {
                $("#form-smart-drinks legend").html("All fields are required");
            }
        }
    });
});

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0);
module.exports = __webpack_require__(1);


/***/ })
/******/ ]);